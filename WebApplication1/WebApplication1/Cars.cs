﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Cars
    {
        public string CarModel { get; set; }
        public int CarYear { get; set; }
        public string CarColour { get; set; }
    }
}
